﻿-- <Migration ID="1b894ec7-35d7-4c96-9ae9-c0cd65f1fd6b" />
GO

PRINT N'Creating [dbo].[DEPT]'
GO
CREATE TABLE [dbo].[DEPT]
(
[deptno] [int] NOT NULL,
[dname] [varchar] (14) NULL,
[loc] [varchar] (13) NULL
)
GO
PRINT N'Creating [dbo].[EMP]'
GO
CREATE TABLE [dbo].[EMP]
(
[empno] [int] NOT NULL,
[ename] [varchar] (10) NULL,
[job] [varchar] (9) NULL,
[mgr] [int] NULL,
[hiredate] [datetime] NULL,
[sal] [numeric] (7, 2) NULL,
[comm] [numeric] (7, 2) NULL,
[dept] [int] NULL
)
GO
PRINT N'Creating primary key [PK__EMP__AF4C318A66538652] on [dbo].[EMP]'
GO
ALTER TABLE [dbo].[EMP] ADD CONSTRAINT [PK__EMP__AF4C318A66538652] PRIMARY KEY CLUSTERED ([empno])
GO
PRINT N'Creating [dbo].[TEST_LOB]'
GO
CREATE TABLE [dbo].[TEST_LOB]
(
[OTC_CD] [varchar] (6) NULL,
[OTC_HQ_CD] [varchar] (6) NULL,
[RCPMNT_FILE] [varchar] (max) NULL
)
GO
